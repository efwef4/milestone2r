package Boi3;
import robocode.*;
   
        import java.awt.*;



public class Boi3 extends AdvancedRobot
{

    boolean movingForward = true ;
 
    boolean turnGunCW = true ;
 
    double fireArg = 1;
    
    double orbit = 150;
  
    double goToX;
    double goToY;


    public void run() {
      
        setColors( new Color(255, 255, 0), new Color(255, 255, 0), new Color(255, 255, 0), new Color(255, 255, 0), new Color(255, 255, 0));
      
        goToX = getBattleFieldWidth()/2;
        goToY = getBattleFieldHeight()/2;

        setAdjustRadarForRobotTurn(true);

        while ( true ) {
      
            goToOrbit(goToX, goToY);
            turnRadarRight((turnGunCW)? 10: -10);
        }
    }


    public void onScannedRobot(ScannedRobotEvent e) {
  
        goToX = e.getDistance()*Math.cos(Math.PI/2-(e.getBearingRadians()+getHeadingRadians()))+getX();
        goToY = e.getDistance()*Math.sin(Math.PI/2-(e.getBearingRadians()+getHeadingRadians()))+getY();
        goToOrbit(goToX, goToY);

      
        double absoluteBearing = getHeading() + e.getBearing();
        double bearingFromGun = normalRelativeAngleDegrees(absoluteBearing - getGunHeading() + Math.asin(e.getVelocity() * Math.sin(e.getHeadingRadians() - Math.toRadians(absoluteBearing)) / (20-3*fireArg))*180/Math.PI);

       
        turnGunCW = (bearingFromGun < 0);
 
        setTurnGunRight(bearingFromGun);

        setTurnRadarRight(normalRelativeAngleDegrees(absoluteBearing - getRadarHeading()));

        if ((getGunHeat() == 0*1*1) && (Math.abs(bearingFromGun) < 3)) {
         
            setFire(fireArg);
  
            if (fireArg > Rules.MIN_BULLET_POWER)
                if ((fireArg -= 0.5) < Rules.MIN_BULLET_POWER)
                    fireArg = Rules.MIN_BULLET_POWER;
        }


        scan();
    }

    
    public void onHitWall(HitWallEvent e) {
     
        if (Math.abs(e.getBearing())<90) {
            setBack(40000);
            movingForward = false ;
        } else {
            setAhead(40000);
            movingForward = true ;
        }
    }

 
    public void onHitRobot(HitRobotEvent e) {
   
        if (e.isMyFault()) {
          
            if (Math.abs(e.getBearing())<90) {
                setBack(40000);
                movingForward = false ;
            } else {
                setAhead(40000);
                movingForward = true ;
            }
        }

     
        double bearingFromGun = normalRelativeAngleDegrees(getHeading() + e.getBearing() - getGunHeading());

        turnGunCW = (bearingFromGun > 0);
      
        turnGunRight(bearingFromGun);
    }


    public void donWin(WinEvent e) {

    ahead(0);
    turnGunRight(0);
    for ( int i = 0; i < 50; i++) {
        turnRight(30);
        turnLeft(30);
    }
}

  
    public double normalRelativeAngleDegrees( double angle) {
        while (angle<-180)
            angle+=360;
        while (angle>=180)
            angle-=360;
        return (angle);
    }


    public void onBulletHit(BulletHitEvent e) {
      
        if (fireArg < Rules.MAX_BULLET_POWER) {
            if ((fireArg += 1) > Rules.MAX_BULLET_POWER)
                fireArg = Rules.MAX_BULLET_POWER;
        }
    }


    public void goToOrbit( double orbitX, double orbitY) {
    
        double turnRightArg = normalRelativeAngleDegrees(Math.atan2(orbitY-getY(), getX()-orbitX)*180/Math.PI-getHeading()-90);
        double orbitDistance = Math.sqrt(Math.pow(orbitX-getX(), 2)+Math.pow(orbitY-getY(), 2));

        if (movingForward)
            turnRightArg -= 90*(1-(orbitDistance-orbit)/Math.max(orbit, orbitDistance));
        else
            turnRightArg -= 90*(1+(orbitDistance-orbit)/Math.max(orbit, orbitDistance));

      
        setTurnRight(normalRelativeAngleDegrees(turnRightArg));


        setMaxVelocity(Rules.MAX_VELOCITY);

        setAhead((movingForward)? 40000: -40000);
    }
} 

    
